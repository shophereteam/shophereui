import React from 'react';
import {
  MDBFooter,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBCol,
  MDBRow,
  MDBBtn
} from 'mdb-react-ui-kit';

export default function Footer() {
  return (
    <MDBFooter className='text-center' color='white' bgColor='dark' style={{height:"30rem"}}>
      <MDBContainer className='p-4'>
        <section className='mb-4'>
          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='facebook-f' />
          </a>

          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='twitter' />
          </a>

          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='google' />
          </a>

          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='instagram' />
          </a>

          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='linkedin-in' />
          </a>

          <a className='btn btn-outline-light btn-floating m-1' href='#!' role='button'>
            <MDBIcon fab icon='github' />
          </a>
        </section>

        <section className=''>
          <form action=''>
            <div className='row d-flex justify-content-center'>
              <div className='col-auto'>
                <p className='pt-2'>
                  <strong>Sign up for our newsletter</strong>
                </p>
              </div>

              <MDBCol md='5' start='12'>
                <MDBInput contrast type='email' label='Email address' className='mb-4' />
              </MDBCol>

              <div className='col-auto'>
                <MDBBtn outline color='light' type='submit' className='mb-4'>
                  Subscribe
                </MDBBtn>
              </div>
            </div>
          </form>
        </section>

        <section className='mb-4'>
            <h3>Shop Here!</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt distinctio earum repellat quaerat
            voluptatibus placeat nam, commodi optio pariatur est quia magnam eum harum corrupti dicta, aliquam
            sequi voluptate quas.
          </p>
        </section>

        <section className=''>
          <MDBRow>
            <MDBCol lg='4' md='6' className='mb-3 mb-md-0'>
              <h5 className='text-uppercase'>Information</h5>

              <ul className='list-unstyled mb-0'>
                <li>
                  <a href='#!' className='text-white'>
                  About Us
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                    Faq
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  Terms & Conditions
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  Contact Us
                  </a>
                </li>
              </ul>
            </MDBCol>

            <MDBCol lg='4' md='6' className='mb-3 mb-md-0'>
              <h5 className='text-uppercase'>Customer Service</h5>

              <ul className='list-unstyled mb-0'>
                <li>
                  <a href='#!' className='text-white'>
                  Payment Methods
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  Money-back
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  Returns
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  Shipping
                  </a>
                </li>
              </ul>
            </MDBCol>

            <MDBCol lg='4' md='6' className='mb-3 mb-md-0'>
              <h5 className='text-uppercase'>Get In Touch</h5>

              <ul className='list-unstyled mb-0'>
                <li>
                  <a href='#!' className='text-white'>
                  Toronto
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  ON, Canada
                  </a>
                </li>
                <li>
                  <a href='#!' className='text-white'>
                  shophere.com
                  </a>
                </li>
              </ul>
            </MDBCol>

          </MDBRow>
        </section>
      </MDBContainer>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2022 Copyright:
        <a className='text-white' href='#!'>
        Copyright © 2022 ShopHere - All Rights Reserved.
        </a>
      </div>
    </MDBFooter>
  );
}
