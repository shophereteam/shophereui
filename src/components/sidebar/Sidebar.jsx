import { useEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import './Sidebar.scss';

import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShopware } from '@fortawesome/free-brands-svg-icons';
const sidebarNavItems = [
  {
    display: 'Menu',
    icon: <i className='bx bx-home'></i>,
    to: '/',
    section: '',
  },
  {
    display: 'My cart',
    icon: <i className='bx bx-cart'></i>,
    to: '/checkout',
    section: 'checkout',
  },
  {
    display: 'Admin',
    icon: <i className='bx bx-user'></i>,
    to: '/admin',
    section: 'admin',
  },
  {
    display: 'Login/Signup',
    icon: <i className='bx bx-exit'></i>,
    to: '/signup',
    section: 'signup',
  },
];

const Sidebar = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [stepHeight, setStepHeight] = useState(0);
  const [mini, setMini] = useState(false);
  const [windowDimension, setWindowDimension] = useState(null);
  const sidebarRef = useRef();
  const indicatorRef = useRef();
  const location = useLocation();

  useEffect(() => {
    setTimeout(() => {
      const sidebarItem = sidebarRef.current.querySelector(
        '.sidebar__menu__item'
      );
      indicatorRef.current.style.height = `${sidebarItem.clientHeight}px`;
      setStepHeight(sidebarItem.clientHeight);
    }, 50);
  }, []);

  // change active index
  useEffect(() => {
    const curPath = window.location.pathname.split('/')[1];
    const activeItem = sidebarNavItems.findIndex(
      (item) => item.section == curPath
    );
    setActiveIndex(curPath.length == 0 ? 0 : activeItem);
  }, [location]);

  const toggleStrikethrough = (e) => {
    e.target.style.width = e.target.style.width == '16rem' ? '6rem' : '16rem';
    setMini(!mini);
  };

  // change view mobile responsively

  useEffect(() => {
    setWindowDimension(window.innerWidth);
  }, []);

  useEffect(() => {
    function handleResize() {
      setWindowDimension(window.innerWidth);
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const isMobile = windowDimension <= 960;

  return (
    <>
      <div
      className='sidebar'
      onMouseOver={toggleStrikethrough}
      onMouseOut={toggleStrikethrough}
    >
        
      
      <div ref={sidebarRef} className='sidebar__menu'>
        <div
          ref={indicatorRef}
          className='sidebar__menu__indicator'
          style={{
            transform: `translateX(-50%) translateY(${
              activeIndex * stepHeight
            }px)`,
          }}
        ></div>
        {sidebarNavItems.map((item, index) => (
          <Link to={item.to} key={index}>
            <div
              className={`sidebar__menu__item ${
                activeIndex === index ? 'active' : ''
              }`}
            >
              <div className='sidebar__menu__item__icon' >{item.icon}</div>
              <div className='sidebar__menu__item__text'>{item.display}</div>
            </div>
          </Link>
        ))}
      </div>
    </div>

    </>
  );
};

export default Sidebar;