import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import validator from 'validator';
import ReCAPTCHA from 'react-google-recaptcha';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(2),

        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '80%',
        },
        '& .MuiButtonBase-root': {
            margin: theme.spacing(2),
        },
    },
}));
let flag = true;
const SignIn = () => {
    const classes = useStyles();
    // create state variables for each input
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setemailError] = useState('');
    const [isVerified, setisVerfied] = useState(false);
    const handleSubmit = (e) => {
            setemailError('');
        e.preventDefault();
        flag = true;
        if ( validateEmail(email)) {
            console.log( email, password);
            
        }
        
        else {
            setemailError("invalid email");
            flag = false;
        }
        //  handleClose();


    };
    const handleClose = (e) => {
        window.location.reload(false);
        window.grecaptcha.reset();
        setEmail('');
        setPassword('');
        setisVerfied('');
        setemailError('');
        console.log("reset")
    };

    const validateEmail = (e) => {

        return validator.isEmail(e);

    };
    const handleOn=()=>{
        setisVerfied(true);
    };


    return (
        <form className={classes.root} onSubmit={handleSubmit}>
            
            
                <TextField
                    label="Email"
                    variant="filled"
                    type="email"
                    required
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                />
                <span className="text-danger">{emailError}</span>

           
            
            <TextField
                label="Password"
                variant="filled"
                type="password"
                required
                value={password}
                onChange={e => setPassword(e.target.value)}
            />
            <div>
            <ReCAPTCHA 
            
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            onChange={handleOn}
            />
            </div>
            <div>
                <Button variant="contained" onClick={handleClose}>
                    Cancel
                </Button>
                <Button type="submit" variant="contained" color="primary" disabled={!isVerified}>
                    Signin
                </Button>
            </div>
        </form>
    );
};

export default SignIn;