import React from 'react';
import { Link } from 'react-router-dom';
import Admin from '../../layouts/Admin/Admin';
import { Button } from '../button/Button';
import './HeroSection.scss';

function HeroSection() {
    return (
        <div className='hero-container'>
            <video src='/videos/video-1.mp4' autoPlay loop muted />
            <p>One stop shop for all your needs</p>
            <div className='hero-btns'>
                <Button className='btns' buttonStyle='btn--outline' buttonSize='btn--large'>
                    Shop Now
                </Button>
                <Button className='btns' buttonStyle='btn--outline' buttonSize='btn--large'>
                    <Link to='/shop' class="btns-link">
                        Explore Products
                    </Link>
                </Button>
            </div>
        </div>
    );
}

export default HeroSection;