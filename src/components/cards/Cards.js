import React from 'react';
import CardItem from '../cardItem/CardItem';

function Cards() {
    return (
        <div className='cards'>
            <h1>List of Products</h1>
            <div className='cards__container'>
                <div className='cards__wrapper'>
                    <div><h3>Food</h3></div><br />
                    <ul className='cards__items'>
                        <CardItem
                            src="images/item-1.jpg"
                            text="Natrel Milk 3%"
                            label="Food"
                            path='#'
                        />
                        <CardItem
                            src="images/item-2.jpg"
                            text="Gala Apple"
                            label="Food"
                            path='#'
                        />
                        <CardItem
                            src="images/item-3.jpg"
                            text="Imported Orange"
                            label="Food"
                            path='#'
                        />
                    </ul><br />
                    <div><h3>Sports</h3></div><br />
                    <ul className='cards__items'>
                        <CardItem
                            src="images/item-4.jpg"
                            text="Skateboard"
                            label="Sports"
                            path='#'
                        />
                        <CardItem
                            src="images/item-5.jpg"
                            text="Roller Blades"
                            label="Sports"
                            path='#'
                        />
                    </ul><br />
                    <div><h3>Entertainment</h3></div><br />
                    <ul className='cards__items'>
                        <CardItem
                            src="images/item-6.jpeg"
                            text="Rubik's Cube 3X3"
                            label="Entertainment"
                            path='#'
                        />
                        <CardItem
                            src="images/item-7.jpg"
                            text="Sony PS5 (Blu-Ray) White"
                            label="Entertainment"
                            path='#'
                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards;