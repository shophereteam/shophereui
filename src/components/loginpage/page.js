import React from 'react';
import { Button } from '../button/Button';
import './page.css';
// import SignUpForm from './components/signupform/signupform';
import { Tab, Tabs } from "react-bootstrap";
import SignUpForm from '../signupform/signupform';
import SignIn from '../signin/singin';
export default function Page() {
    // const page = () => {
    //     return <SignUpForm />;
    //   };
    return (
        
            <div className='contain-1'>
                <div className='contain-2'>

                    <Tabs
                        //   id="controlled-tab-example"
                        //   activeKey={this.state.signValue}
                        //   onSelect={key => this.setState({ signValue: key })}
                        className="justify-content-center"
                    >
                        
                        <Tab eventKey="signup" title="Sign up"  >
                           
                        <div className='contain-3'> 
                            <SignUpForm />
                            </div>
                        </Tab>
                        
                        <Tab eventKey="signin" title="Sign In" >
                        <div className='contain-3'>
                            <SignIn />
                            </div>
                        </Tab>
                        
                        {/* <Tab eventKey="forgotpassword" title="Forgot Password?">
                        <ForgotPassword /> */}
                        {/* </Tab> */}
                    </Tabs>
                    </div>
       

            </div>

        
    );
}
