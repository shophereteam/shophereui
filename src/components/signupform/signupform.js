import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';
import validator from 'validator';
import ReCAPTCHA from 'react-google-recaptcha';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: theme.spacing(2),

        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '80%',
        },
        '& .MuiButtonBase-root': {
            margin: theme.spacing(2),
        },
    },
}));
let flag = true;
const SignUpForm = () => {
    const classes = useStyles();
    // create state variables for each input
    const [firstName, setFirstName] = useState('');
    const [title, setTitle] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [phone, setPhone] = useState('');
    const [emailError, setemailError] = useState('');
    const [phoneError, setphoneError] = useState('');
    const [isVerified, setisVerfied] = useState(false);


    const handleSubmit = (e) => {
        setphoneError('');
        setemailError('');
        e.preventDefault();
        flag = true;
        if (validatePhone(phone) && validateEmail(email)) {
            console.log(firstName, lastName, email, password, phone,title);

        }
        if (validatePhone(phone)==false) {
            setphoneError("invalid phone number");
            flag = false;
        }
        if (validateEmail(email)==false) {
            setemailError("invalid email");
            flag = false;
        }
        //  handleClose();


    };
    const handleClose = (e) => {
        window.grecaptcha.reset();
        setFirstName('');
        setLastName('');
        setEmail('');
        setPassword('');
        setPhone('');
        setphoneError('');
        setemailError('');
        setisVerfied('');
        setTitle('');
        // <ReCAPTCHA 
        // sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
        // onChange={()=>{}}
        // />
    };

    const validateEmail = (e) => {

        // console.log("hi")
        // console.log(validator.isEmail(e))
        return validator.isEmail(e);

    };
    const validatePhone = (e) => {

        return validator.isMobilePhone(e);

        // setPhone(e);
    };
    const handleOn=()=>{
        setisVerfied(true);
    };


    return (
        <form className={classes.root} onSubmit={handleSubmit}>
            <TextField
                label="Title"
                variant="filled"
                select
                value={title}
                onChange={e => setTitle(e.target.value)}
            >
               <MenuItem key="mr" value="mr">
              mr
            </MenuItem>
            <MenuItem key="mrs" value="mrs">
              mrs
            </MenuItem>
            </TextField>
            
            <TextField
                label="First Name"
                variant="filled"
                required
                value={firstName}
                onChange={e => setFirstName(e.target.value)}
            />
            <TextField
                label="Last Name"
                variant="filled"
                required
                value={lastName}
                onChange={e => setLastName(e.target.value)}
            />

            <TextField
                label="Email"
                variant="filled"
                type="email"
                required
                value={email}
                onChange={e => setEmail(e.target.value)}
            />
            <span className="text-danger">{emailError}</span>



            <TextField
                label="Phone Number"
                variant="filled"
                type="phone"
                required
                value={phone}
                onChange={e => setPhone(e.target.value)}
            />
            <span className="text-danger">{phoneError}</span>


            <TextField
                label="Password"
                variant="filled"
                type="password"
                required
                value={password}
                onChange={e => setPassword(e.target.value)}
            />
            <div>
            <ReCAPTCHA 
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            onChange={handleOn}
            />
            </div>
            <div>
                <Button variant="contained" onClick={handleClose}>
                    Cancel
                </Button>
                <Button type="submit" variant="contained" color="primary" disabled={!isVerified}>
                    Signup
                </Button>
            </div>
        </form>
    );
};

export default SignUpForm;