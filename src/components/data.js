
export const categories = [
    {
        id: 1,
        img:"https://images.lululemon.com/is/image/lululemon/LM3DAHS_020111_2?wid=1600&op_usm=0.5,2,10,0&fmt=webp&qlt=80,1&fit=constrain,0&op_sharpen=0&resMode=sharp2&iccEmbed=0&printRes=72",
        title:"Men",
    },
    {
        id: 2,
        img:"https://img.ltwebstatic.com/images3_pi/2021/01/11/1610349490def6a49499811c929702c74ff904f55c_thumbnail_900x.webp",
        title:"Kids",
    },
    {
        id: 3,
        img:"https://images.lululemon.com/is/image/lululemon/LW3ETES_050706_1?wid=1600&op_usm=0.5,2,10,0&fmt=webp&qlt=80,1&fit=constrain,0&op_sharpen=0&resMode=sharp2&iccEmbed=0&printRes=72",
        title:"Women",
    },
];

