import React, { useEffect, useState } from 'react';
import '../../App.scss';
import Cards from '../../components/cards/Cards';
import HeroSection from '../../components/hero/HeroSection';
import Footer from '../../components/footer/Footer';
import Categories from '../../components/Category/Categories';
import CardItem from '../../components/cardItem/CardItem';

function Home() {

    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)

    useEffect(() => {
        fetch(`http://localhost:8080/item/get`)
      .then((response) => {
        if (response.status !== "200") {
          let err = Error;
          err.message = "Invalid response code: " + response.status;
         setError(err)
        }
        return response.json();
      })
      .then((json) => {
        setData(json)
        setLoading(true)
      }).catch((error) => {
        console.log('error ', error);
        setError(error)
    }).finally(() => {
        setLoading(false)
    });
    },[]);
    
    return (
        <>
            <HeroSection />
           
            <div className="container"><Categories /></div>
            {data ?
           ( data.map((item, index) => {
               <CardItem
                    key={index}
                    src={item.ImageUrl}
                    text={item.ItemName}
                    label={item.PrimaryCategoryObject.Name}
                    path='/'
                />
           }))  :
            <Cards />}
        </>
    )
}

export default Home;
