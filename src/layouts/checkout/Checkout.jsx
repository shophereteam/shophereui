import { Input } from 'antd';
import { useForm } from 'react-hook-form';
import React from 'react';
import { useState } from 'react';
// import {Nav} from 'react-bootstrap';

function Checkout() {
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [data, setData] = useState(null)
  const [email, setEmail] = useState(null)

  const onSubmit = (data) => {
    console.log(data)
  }
  const onChange = (e) => {
    setData(e.target.value)
    setEmail(e.target.value)
    console.log(e.target.value)
  }
  return <div>
    <div className='container'>
    <nav >
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <button class="nav-link" id="nav-shipping-tab" data-bs-toggle="tab" data-bs-target="#nav-shipping" type="button" role="tab" aria-controls="nav-shipping" aria-selected="true">SHIPPING</button>
    <button class="nav-link" id="nav-payment-tab" data-bs-toggle="tab" data-bs-target="#nav-payment" type="button" role="tab" aria-controls="nav-payment" aria-selected="false" disabled>PAYMENT</button>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-shipping" role="tabpanel" aria-labelledby="nav-shipping-tab">

<h2>1. SHIPPING & BILLING INFORMATION</h2>
<div className="mb-3">
      <label className="form-label">
        Email
      </label>
      <input
        type='text'
        className={`email`}
        id='email'
        name='email'
        value={data}
        onChange={onChange}
        placeholder="Email"
      />
    </div>
<p>{data}</p>

<form onSubmit={handleSubmit(onSubmit)}>
      {/* register your input into the hook by invoking the "register" function */}
      <input defaultValue="test" {...register("example")} />
      
      {/* include validation with required or other standard HTML validation rules */}
      <input {...register("exampleRequired", { required: true })} />
      {/* errors will return when field validation fails  */}
      {errors.exampleRequired && <span>This field is required</span>}
      
      <input type="submit" />
    </form>
  </div>
  <div class="tab-pane fade" id="nav-payment" role="tabpanel" aria-labelledby="nav-payment-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita ipsum, reprehenderit officiis tenetur rem architecto fugit cupiditate consequuntur explicabo velit similique iure debitis aut asperiores ullam recusandae eos amet porro.
  </div>
</div>
</div>
  </div>;
}

export default Checkout;
