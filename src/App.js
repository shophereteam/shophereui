import './App.scss';
import Navbar from './components/navbar/Navbar';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Home from './layouts/home/Home';
import Sidebar from './components/sidebar/Sidebar';

import SignUpForm from './components/signupform/signupform';
import Page from './components/loginpage/page';

import Admin from './layouts/Admin/Admin';

import 'boxicons/css/boxicons.min.css';

import { useEffect, useState } from 'react';
import Checkout from './layouts/checkout/Checkout';
import Footer from './components/footer/Footer';
function App() {
  const [windowDimension, setWindowDimension] = useState(null);
  const mobileStyle = {
    paddingTop: '80px',
  }
  
  useEffect(() => {
    setWindowDimension(window.innerWidth);
  }, []);
  
  useEffect(() => {
    function handleResize() {
      setWindowDimension(window.innerWidth);
    }
  
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  
  const isMobile = windowDimension <= 960;
  return (
    <div className="App">

      <Router>
      <Navbar isMobile={isMobile} />

{!isMobile && <Sidebar />}
      <div className="content">

        <Routes > 
          <Route index element={<Home />} />
          <Route path="/checkout" exact element={<Checkout />} />
          <Route path="/sign" exact element={<SignUpForm />} />
          <Route path="/signup" exact element={<Page />} />
          <Route path="/admin" element={<Admin />} />
        </Routes>
      </div>
      </Router>
      <Footer/>
    </div>
  );
}

export default App;
